#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setupgraph();
    ~MainWindow();


	void histo(string names[], 
		double values[], int size, string xAxisLabel, string yAxisLabel);


private:
    Ui::MainWindow *ui;

    QCustomPlot *myPlot;   
};

#endif // MAINWINDOW_H
