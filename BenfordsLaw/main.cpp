#include "mainwindow.h"
#include <QApplication>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>

using namespace std;


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.setWindowTitle("Benford\'s Law");
    w.show();
    
    // Here is a simple example to help you understand how to
    // invoke the histo method.

    // This is an array of strings
    string histoNames[4]  = {"Rosa", "Pepin", "Lobo", "Mota"};

    // This is an array of corresponding values
    double histoValues[4] = {4, 5, 1, 9};

    // We pass the array of strings, the array of values, the size of the
    // arrays, and the names of the x and y axis.

    w.histo(histoNames, histoValues, 4, "personaje", "frecuencia");

    return a.exec();
}
