#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"
#include <cmath>
using namespace std;



///
/// \brief MainWindow::MainWindow: just a lowly constructor
/// \param parent: a pointer to this objects parent.
///

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

///
/// \brief MainWindow::histo: this function receives information about the ticks and
///        values for a bar chart, then displays it using the customPlot widget
///        from qcustomplot.
/// \param names: an array of strings for the names of the x-axis ticks
/// \param values: an array of doubles for the values of y-axis
/// \param size: the size of the names (and values) arrays
/// \param xAxisLabel: a string for the x-axis title
/// \param yAxisLabel: a string for the y-axis title
///

void MainWindow::histo(
        std::string names[], double values[], int size, string xAxisLabel, string yAxisLabel)
{

    QCustomPlot *customPlot = ui->customPlot;
    QString demoName = "Bar Chart Demo";
    QCPBars *barChart = new QCPBars(customPlot->xAxis, customPlot->yAxis);

    customPlot->addPlottable(barChart);

    QPen pen;
    pen.setWidthF(1.2);
    //barChart->setName("Fossil fuels");
    pen.setColor(QColor(255, 131, 0));
    barChart->setPen(pen);
    barChart->setBrush(QColor(255, 131, 0, 50));


    // prepare x axis with labels:
    QVector<double> ticks;
    QVector<QString> labels;
    for (int i = 1; i <= size; i++) {
        ticks.push_back(i);
        labels.push_back(QString(names[i-1].c_str()));
    }

    // prepare y axis:
    customPlot->xAxis->setAutoTicks(false);
    customPlot->xAxis->setAutoTickLabels(false);
    customPlot->xAxis->setTickVector(ticks);
    customPlot->xAxis->setTickVectorLabels(labels);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTickCount(0);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->setRange(0, size+1);
    customPlot->xAxis->setLabel(QString(xAxisLabel.c_str()));

    // prepare y axis:
    customPlot->yAxis->setRange(0, *std::max_element(values,values+size));
    customPlot->yAxis->setPadding(5); // a bit more space to the left border
    customPlot->yAxis->setLabel(QString(yAxisLabel.c_str()));
    customPlot->yAxis->grid()->setSubGridVisible(true);
    QPen gridPen;
    gridPen.setStyle(Qt::SolidLine);
    gridPen.setColor(QColor(0, 0, 0, 25));
    customPlot->yAxis->grid()->setPen(gridPen);
    gridPen.setStyle(Qt::DotLine);
    customPlot->yAxis->grid()->setSubGridPen(gridPen);

    // Add data:
    QVector<double> barChartData;
    for (int i = 0; i < size; i++) {
        ticks.push_back(i);
        barChartData.push_back(values[i]);
    }
    barChart->setData(ticks, barChartData);
    this->resize(customPlot->width(),customPlot->height());

}

///
/// \brief MainWindow::~MainWindow: destructor!
///

MainWindow::~MainWindow()
{
    delete ui;
}



