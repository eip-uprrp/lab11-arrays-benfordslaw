#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"
#include <cmath>

// Esta es la función que usted modificará. Su labor es añadir
// expresiones para las coordenadas x y y según se le indica en
// las instrucciones del laboratorio.

void MainWindow::setupgraph()
{
    double y = 0.00;
    double x = 0.00;
    double increment = 0.01;

    for (double t = 0; t < 2*M_PI; t = t + increment) {
        // corazón
//        x =  16 * pow(sin(theta),3.0);
//        y =  13 * cos(theta) - 5*cos(2*theta) - 2*cos(3*theta) - cos(4*theta) - 3;

        x = t;
        y = t;
        // mariposa
//         double r = pow(sin(1.2*theta), 2) + pow(cos(6*theta), 3);
//         x = 10 * r * cos(theta);
//         y = 10 * r * sin(theta);

        // añadimos la x y la y como un punto más de la gráfica
        AddPointToGraph(x,y);
    }

    // Una vez hemos añadido todos los puntos graficamos
    Plot();
}
