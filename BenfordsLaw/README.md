

## Lab ??: Benford's Law

### Objectives:

At the end of this lab, the students will have:

* practiced using an array of counters to determine the frequency of data in a file
* used Benford's Law to determine bogus data according to the frequency distribution.

Concepts that the students should review before the lab session:

* arrays
* counters
* conversion between integers, strings, characters
* string functions


As part of your new job as IT auditor you suspect that someone in the Chicago Transit Authority (CTA) has been tampering with the information systems and changing the data files that contain the bus route daily totals. You are given five sequential files that contain daily totals for each of the bus routes of the CTA and must determine if one or more of the files contain bogus data. In this lab you will implement a program that will help you which of the file(s) contain bogus data using Benford's Law, a property that is observed in many real-life sources of data .


### What is Benford’s Law? (copied from the ISACA journal)

Benford’s Law, named for physicist Frank Benford, who worked on the theory in 1938, is the mathematical theory of leading digits. Specifically, in data sets, the leading digit(s) is (are) distributed in a specific, nonuniform way. While one might think that the number 1 would appear as the first digit 11 percent of the time (i.e., one of nine possible numbers), it actually appears about 30 percent of the time (see figure 1). The number 9, on the other hand, is the first digit less than 5 percent of the time. The theory covers the first digit, second digit, first two digits, last digit and other combinations of digits because the theory is based on a logarithm of probability of occurrence of digits.

---

![](http://i.imgur.com/cc7fGom.png)

**Figure 1**. Taken from [2] 

---


### How to use Benford's Law to spot bogus data


In this lab we will use Benford's Law applied only to the leading digit. To do this, we need to determine the frequency of each leading digit in the numbers of the file. Say that you are given a file that contains the following integer numbers:

```
890 3412 234 143 112 178 112 842 5892 19 
777 206 156 900 1138 438 158 978 238 192
```

As you read each number $n$, you determine its leading digit (how to extract the leading digit is left as an exercise for you). You must also keep track of how many times the same leading digit appears in the data set. The easiest way to  "simulatenously" keep track of how many times you find a leading 1, leading 2, ... leading 9, is to use an *array of counters*. This *array of counters* is simply an array of integers where each element is incremented whenever we find a certain leading digit. For instance, for this exercise our array of counters can be an array of 10 integers, initialized to 0. 

----

![](http://i.imgur.com/wRMASYI.png)

**Figure 2**. Array of 10 integers initialized to 0.

----


Each time a leading digit $d$ is found, the element with index $d$ is incremented. For example, after reading the numbers  890 3412 234 143 112, the contents of the array would be as follows:

----

![](http://i.imgur.com/0F16Z5f.png)

**Figure 3**. Contents of the array after reading 890, 3412, 234, 143, 112 and counting their leading digits.

---


At the end the content of each element will be the number of times that digit appears in the data.

---

![](http://i.imgur.com/hQ3axk9.png)

**Figure 4**. Contents of the array after reading the rest of the data.

---

The frequency of occurrence is defined as the ratio of times that a digit appears over the total number of data.  For example, the frequency of leading digit $1$ in the example would computed as $ 9 / 20 = 0.45$ .  The **histogram** is the preferred visualization for frequency distributions. In essence, it is a bar chart where the y-axis is the frequency and a bar is drawn for each of the counted classifications (in our case, each digit). 

---

![](http://i.imgur.com/UnFU9FO.png)

**Figure 4**. Histogram for the frequency of leading digits in the sample data

---

### Exercise 

Download the files for this exercise from $___$

The data files `cta-a.txt`, `cta-b.txt`,  `cta-c.txt`,  `cta-d.txt`,  and `cta-e.txt` in the data directory contain either real or bogus data. Each line of the files specifies a bus route number follwed the number of people that used the bus a given day. Open the file `cta-a.txt` to understand the format of the data. This will be important when you read the data from your program.

Now open `BenfordsLaw.pro` with QtCreator and open the `main.cpp` file. Study the main function to make sure that you understand its various parts. In essence, the provided main function creates a window and paints a histogram like the one shown in Figure 5. 

![](http://i.imgur.com/Lj8F1hP.png)

**Figure 5**. Output of the provided program. It displays a histogram using data from arguments  `histoNames` and `histoValues`.

Using the provided main function as inspiration, you and your partner will add functionality to read files like the ones provided and determine the frequency of leading digits in the data that appears in the second columns of the files. Once your program has obtained the frequencies of the leading digits it will use the `histo` method to display a histogram.  Based on the distribution of frequencies for the data in each of the files, you will be able to determine if the file contains real or fake data. 


### Deliverables:

1. Contents of the `main.cpp` file (you do not need to change the code in any of the other files).

2. A pdf that shows the screenshots of the histograms for each of the files and your decisions as to whether the file contained real or bogus data.



[2] http://www.isaca.org/Journal/Past-Issues/2011/Volume-3/PublishingImages/11v3-understanding-and.jpg 






