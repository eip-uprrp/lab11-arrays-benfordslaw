
#Lab. 11: Arreglos - Benford's Law

<img src="http://i.imgur.com/UnUXImq.png" width="215" height="174">   <img src="http://i.imgur.com/cc7fGom.png" width="215" height="174">  <img src="http://i.imgur.com/wZFfv0G.png" width="215" height="150">


<p> </p>
Los arreglos te permiten guardar y trabajar con varios datos del mismo tipo. Los datos se guardan en espacios de memoria consecutivos a los que se puede acceder utilizando el nombre del arreglo e índices o suscritos que indican la posición en que se encuentra el dato. Es fácil acceder a los elementos de un arreglo utilizando ciclos.  En la experiencia de laboratorio de hoy practicarás el uso de arreglos y contadores para implementar un programa en el que usarás la Ley de Benford para ayudarte  a detectar   archivos con datos falsos.




##Objetivos:

1. Practicar el  uso de un arreglo de contadores para determinar la frecuencia de los datos de un archivo.

2. Detectar el uso de datos falsos utilizando la distribución de frecuencia y la Ley de Benford




##Pre-Lab:

Antes de llegar al laboratorio debes:


1. aprender como extraer el dígito líder (primer dígito) de un número. 

2. haber repasado los conceptos relacionados arreglos y contadores.

3. haber repasado las funciones asociadas a "strings".

4. haber repasado como obtener datos de un archivo.

4. haber repasado las conversiones entre tipos de datos enteros, "strings" y caracteres.

5. haber estudiado los conceptos e instrucciones de la sesión de laboratorio.

6. haber tomado el [quiz Pre-Lab 11](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7640) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).

---
---

Como parte de tu nuevo trabajo de auditor de tecnología de información, tienes la sospecha de que alguien en la Autoridad Metropolitana de Autobuses (AMA) de Puerto Rico ha estado manipulando los sistemas de información y cambiando los archivos de datos que contienen los totales de pasajeros de las rutas diarias de las guaguas. Te han dado 5 archivos en secuencia que contienen los totales diarios para cada una de las rutas de las guaguas de la AMA y debes determinar si uno o más archivos contienen datos falsos. En esta experiencia de laboratorio implementarás un programa en el que usarás la Ley de Benford, una propiedad que se observa en muchos datos de la vida real, para ayudarte  a detectar cuál(es)  archivos tienen datos falsos.



## ¿Qué es la Ley de Benford? (adaptado del ISACA Journal [1])


La Ley de Benford es la teoría matemática de los dígitos líderes de un número, y fue llamada así en honor al físico Frank Benford, quién trabajó en esta teoría en 1938. Específicamente, en conjuntos de datos, los dígitos líderes están distribuidos en una manera específica, no uniforme. Uno podría pensar que el número 1 aparece como primer dígito el 11% del tiempo (esto es, uno de 9 números posibles), sin embargo, este número aparece como líder alrededor del 30% del tiempo (vea la Figura 1). Por otro lado, el número 9 es el primer dígito menos del 5% del tiempo. La teoría cubre las ocurrencias del primer dígito, el segundo dígito, los primeros dos dígitos, el último dígito y otras combinaciones de dígitos porque la teoría está basada en un logaritmo de probabilidad de ocurrencia de dígitos.


---

![](http://i.imgur.com/cc7fGom.png)

**Figura 1.** Distribución del primer dígito según la Ley de Benford. Tomado de [1] 

---


## Cómo usar la Ley de Benford para detectar datos falsos 

En esta experiencia de laboratorio usarás la Ley de Benford aplicada solamente al primer dígito. Para hacer esto, necesitamos determinar la frecuencia de cada dígito líder en los números en un archivo. Supón que te dan un archivo que contiene los siguientes números enteros:




```
890 3412 234 143 112 178 112 842 5892 19 
777 206 156 900 1138 438 158 978 238 192
```


Según vas leyendo cada número $n$, determinas su dígito líder (la manera de extraer el dígito líder de un número se deja como un ejercicio para tí). También debes estar pendiente de cuántas veces sale el dígito líder en el conjunto de datos. La manera más fácil para darle seguimiento "a la misma vez" a cuántas veces aparece el 1 como líder, el 2 como líder, ... , el 9 como líder, es utilizando un *arreglo de contadores*. Este arreglo de contadores es sencillamente un arreglo de enteros en el que cada elemento se incrementa cada vez que encontramos cierto dígito líder. Por ejemplo, para este ejercicio nuestro arreglo de contadores puede ser un arreglo de 10 enteros, inicializado a `0`.



----

![](http://i.imgur.com/wRMASYI.png)

**Figura 2.** Arreglo de 10 enteros inicializado a `0`.


----

Cada vez que se encuentra un dígito líder `d`, el elemento con índice `d` se incrementa. Por ejemplo, luego de leer los números `890` `3412` `234` `143` `112`, el contenido del arreglo sería como sigue:


----

![](http://i.imgur.com/0F16Z5f.png)

**Figura 3.** Contenido del arreglo luego de leer `890` `3412` `234` `143` `112`, y contar sus dígitos líderes.

---

Al finalizar de examinar el archivo, el contenido de cada elemento en el arreglo será el número de veces que el dígito aparece en los datos.



---

![](http://i.imgur.com/hQ3axk9.png)

**Figura 4.** Contenido del arreglo luego de examinar todos los datos. 

---


La **frecuencia de ocurrencia** se define como la razón del número de veces que un dígito aparece sobre el número total de datos. Por ejemplo, la frecuencia del dígito líder `1` en el ejemplo de la Figura 4 se computa como $9/20 = 0.45$. La manera común de visualizar las distribuciones de frecuencia es utilizando un **histograma**. En escencia es una gráfica de barras en la que el eje de $y$ es la frecuencia de ocurrencia y se dibuja una barra para cada una de las clasificaciones que se cuenta (en nuestro caso, una barra para cada dígito líder).


---

![](http://i.imgur.com/UnFU9FO.png)

**Figura 5.** Histograma para la frecuencia de los dígitos líderes en los datos de muestra.

---

##Sesión de laboratorio:

**Instrucciones**


1. Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab11-arrays-benfordslaw.git` para descargar la carpeta `Lab11-Arrays-BenfordsLaw` a tu computadora.

2. Los archivos de datos  `cta-a.txt`, `cta-b.txt`,  `cta-c.txt`,  `cta-d.txt`,  y `cta-e.txt` en el directorio `data` contienen datos reales o datos falsos. Cada línea del archivo especifica el número de una ruta de guagua seguido por el número de personas que usaron la guagua en cierto día. Abre el archivo  `cta-a.txt` para que entiendas el formato de los datos. Esto será importante cuando leas los datos desde tu programa.


3.  Haz doble "click" en el archivo `BenfordsLaw.pro` para cargar este proyecto a Qt y abre el archivo `main.cpp`. Estudia la función `main` para que te asegures de que entiendes todas las partes. En escencia, la función `main` que te proveemos crea una pantalla y dibuja un histograma parecido al que se muestra en la Figura 6.


    ![](http://i.imgur.com/Lj8F1hP.png)

    **Figura 6.** Salida del programa que se provee. Se despliega un histograma utilizando datos de los argumentos   `histoNames` e `histoValues`.

4. Utilizando como inspiración la función `main` que te proveemos, tu y tu pareja le añadirán funcionalidad para leer archivos como los provistos y determinar la frecuencia de dígitos líderes en los datos que aparecen en las segundas columnas de los archivos. Una vez tu programa haya obtenido las frecuencias de los dígitos líderes, utilizará el método `histo` para desplegar un histograma. Basado en la distribución de frecuencia de los dígitos líderes en los datos en cada uno de los archivos, podrás determinar si el archivo contiene datos reales o datos falsos.



## Entregas:

1. Entrega el archivo `main.cpp` utilizando este [enlace a Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7641). No necesitas cambiar el código de ningún otro archivo. Recuerda comentar adecuadamente las funciones, usar indentación adecuada y buenas prácticas para darle nombre a las variables.


2. Entrega un `pdf` que muestre fotos de los histogramas para cada uno de los archivos y tus decisiones sobre si el archivo contiene o no datos falsos. Utiliza [este enlace](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7642).




[1] http://www.isaca.org/Journal/archives/2011/Volume-3/Pages/Understanding-and-Applying-Benfords-Law.aspx






